﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyAvailability.Models;

namespace MyAvailability.Controllers
{
    public class AvailabilitiesController : Controller
    {
        private EmployeeContext employeeContext = new EmployeeContext();

        // GET: Availabilities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Availability> availability =
                employeeContext.Availabilities
                .Where(a => a.EmployeeID == id)
                .ToList();
            if (availability.Count <= 0)
            {
                return HttpNotFound();
            }
            return View(availability);
        }

        // GET: Availabilities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Availability> availabilities = new List<Availability>();
            for (int i = 0; i < 7; i++)
            {
                availabilities.Add(employeeContext.Availabilities.Find(id, i));
            }
            if (availabilities.Count <= 0)
            {
                return HttpNotFound();
            }
            return View(availabilities);
        }

        // POST: Availabilities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IEnumerable<Availability> availabilities)
        {
            if (ModelState.IsValid)
            {
                foreach (Availability availability in availabilities)
                {
                    employeeContext.Entry(availability).State = EntityState.Modified;
                }
                employeeContext.SaveChanges();
                return RedirectToAction("Details", new { id = availabilities.FirstOrDefault().EmployeeID });
            }
            return View(availabilities);
        }
    }
}
