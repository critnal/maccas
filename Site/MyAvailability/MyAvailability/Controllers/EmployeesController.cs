﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyAvailability.Models;

namespace MyAvailability.Controllers
{
    public class EmployeesController : Controller
    {
        private EmployeeContext employeeContext = new EmployeeContext();

        // GET: Employees
        public ActionResult Index()
        {
            return View(employeeContext.Employees.ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeContext.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        public ActionResult Availability(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Availability> availability =
                employeeContext.Availabilities
                .Where(a => a.EmployeeID == id)
                .ToList();
            if (availability.Count <= 0)
            {
                return HttpNotFound();
            }
            return View(availability);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeID,FirstName,LastName,DateOfBirth,IsAdmin")] Employee employee)
        {
            if (ModelState.IsValid && employee.EmployeeID != 0)
            {
                // Add the employee
                employeeContext.Employees.Add(employee);
                employeeContext.SaveChanges();

                // Add an availability for the employee

                for (int i = 0; i < 7; i++)
                {
                    employeeContext.Availabilities.Add(
                        new Availability()
                        {
                            EmployeeID = employee.EmployeeID,
                            Day = i,
                            StartTime = DateTime.Now,
                            EndTime = DateTime.Now
                        }
                    );
                }
                employeeContext.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeContext.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeID,FirstName,LastName,DateOfBirth,IsAdmin")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employeeContext.Entry(employee).State = EntityState.Modified;
                employeeContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeContext.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = employeeContext.Employees.Find(id);
            employeeContext.Employees.Remove(employee);
            employeeContext.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                employeeContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
