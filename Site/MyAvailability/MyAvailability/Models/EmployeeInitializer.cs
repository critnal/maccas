﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MyAvailability.Models
{
    public class EmployeeInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<EmployeeContext>
    {
        protected override void Seed(EmployeeContext context)
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    EmployeeID = 67135,
                    FirstName = "Alex",
                    LastName = "Crichton",
                    DateOfBirth = DateTime.Parse("01/02/1990"),
                    IsAdmin = true
                }
            };
            employees.ForEach(s => context.Employees.Add(s));
            context.SaveChanges();

            var availabilities = new List<Availability>();
            for (int i = 0; i < 7; i++)
                availabilities.Add(new Availability
                {
                    EmployeeID = 67135,
                    Day = i,
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Now
                });
            availabilities.ForEach(s => context.Availabilities.Add(s));
            context.SaveChanges();
        }
    }
}