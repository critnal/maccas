﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyAvailability.Startup))]
namespace MyAvailability
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
